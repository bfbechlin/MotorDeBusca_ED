CC=gcc
CFLAGS=-c

all: build

build: main.o busca.o
	$(CC) main.o busca.o -o busca

main.o: main.c
	$(CC) $(CFLAGS) main.c

ball.o: busca.c
	$(CC) $(CFLAGS) busca.c

clean:
	rm *o busca
