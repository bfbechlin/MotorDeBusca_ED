#include "busca.h"

/* Funcoes */
PT_ARVORE* inicializaArvore(){
    return NULL;
}

PT_ARVORE* insereNodo(PT_ARVORE *r, const char *novo){
    char* chPointer;
    if(r){
        if(strcmp(novo, r->dados.palavra) > 0) // novo eh maior que r->str -> insere novo dir
            r->dir = insereNodo(r->dir, novo);
        else if(strcmp(novo, r->dados.palavra) < 0) // novo eh menor que r->str -> insre novo esq
            r->esq = insereNodo(r->esq, novo);
		else // novo == r->str
    		r->dados.frequencia ++;
	}
    else{
        r = (PT_ARVORE*)malloc(sizeof(PT_ARVORE));
        chPointer = (char*)malloc(strlen(novo)+1);
        strcpy(chPointer, novo);
        r->dados.palavra = chPointer;
		r->dados.frequencia = 0;
        r->esq = NULL;
        r->dir = NULL;
    }
    return r;
}

void imprimeOrd(PT_ARVORE *r){
	if(r){
		imprimeOrd(r->esq);
		printf("%s ", r->dados.palavra);
		imprimeOrd(r->dir);
	}
}
