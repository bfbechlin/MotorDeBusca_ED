# Sugestão de Consultas em Motores de Busca

**Autores:** Béuren Bechlin, Lucas Leal

__INF01203 - Estrutura de Dados__ 

Prof. Renata Galante, Prof. Viviane Moreira  DATA: 08/12/2015

Projeto disponível em GPL v3

---

## Introdução

Nesse trabalho será abordada a implementação de um motor de busca para aplicarmos os conhecimentos desenvolvidos na disciplina. 
O exercício proposto foi uma função de autocompletar para um motor de busca, ou seja, tendo em vista a inserção de uma palavra qualquer indicar
qual é uma possível próxima palavra levando em consideração um corpus previamente carregado.

A seguinte formula estabelece a estatística de associação das palavras:


## Escolha das Estruturas

Para estruturar o corpus de maneira a facilitar a busca nós utilizamos uma AVL onde cada nodo, além dos dois ponteiros usuais para os filhos
esquerdo e direito, possui um ponteiro para uma lista duplamente encadeada.

As chaves de ordenação da AVL são as palavras do corpus e as entradas da lista são as palavras que seguem a chave do nodo ao longo do texto. 
Para utilizar melhor o espaço em disco optamos por salvar um ponteiro para o nó que contenha a palavra correspondente, visto que esse nó já
estará criado quando for salvo na lista. Dessa forma além de diminuir o espaço em disco temos acesso rápido as frequências individuais para
posteriormente calcular a estatística necessária para estabelecer a ordem de sugestão.

## Funcionamento da Aplicação

### Montando a Estrutura
Inicialmente carrega-se o corpus para uma AVL e registra-se as relações entre as palavras encontradas no corpus. 
Para isso, o texto é lido linha por linha do arquivo onde contem o corpus e as strings são separadas usando _strtok_ para serem inseridas na árvore.
A AVL é ordenada alfabeticamente usando o retorno da função 'strcmp'. Para relacionar a palavra atual com a sua seguinte usamos o seguinte algoritmo:

* Insere a palavra corrente na árvore 
 1. Se _strcmp > 0_ Cria um novo nó à direita
 2. Se _strcmp < 0_ Cria um novo nó à esquerda
 3. Se _strcmp = 0_ Palavra já está na lista, altera a frequência
* Salva o ponteiro da palavra inserida na iteração anterior na lista de sugestões.
 1. Se  _strcmp != 0_ Cria um novo elemento na lista
 2. Se _strcmp = 0_ Palavra já está na lista, altera a frequência
* Guarda o ponteiro da palavra corrente para a próxima iteração. Esse ponteiro pode ser encontrado na implementação como _nodoAnterior_.

Esse algoritmo realiza toda a criação da árvore de palavras como também as listas de sugestões para cada nó (palavra). 
As palavras da lista são inseridas usando _strcmp_ da mesma maneira que os nodos da AVL.

### Calculando as Estatísticas e Ordenando as Listas
Após essa etapa concluída é realizada a etapa de cálculo de estátisticas, onde se passa em todos os nós da árvore e se calcula a expressão de associação 
de palavras. Esse algoritmo é realizado facilmente já que em nossa implementação possuímos a frequência mútua na própria lista e também temos as frequências individuais, um na palavra que contém a sugestão e outro na palavra sugerida.

A próxima etapa é ordenar todas as listas de sugestões de todos os nós da árvore, para isso temos duas implementações de _sorts_, o _merge sort_ e 
o _bubble sort_. Nas duas implementações foram desenvolvidas para ordenar em ordem decrescente em relação a estatistíca, entretanto quando existem estatistícas iguais é ordem por ordem lexicográfica usando _strcmp_. 
Após testes verificamos como era esperado uma velocidade maior de execução do algoritmo do _merge sort_ em relação a _bubble sort_ de aproximadamente 15% 
e por esse motivo estamos usando o algoritmo de _merge sort_.

### Consultas
As consultas são realizadas trivialmente com um algoritmo catacterístico de pesquisa binária, quando encontra a palavra em questão imprime a quantidade 
de palavras sugeridas que foi solicitada ou até encontrar um nó _NULL_ na lista. 

## Considerações Finais

Um detalhe interessante do projeto é que foi criado um arquivo _MakeFile_ disponível para que seja realizada a compilação e linkagem dos sources, 
headers e demais bibliotecas que foram usados e criadas. Na dependência _CC_ do _MakeFile_ está disponível a opção de qual compilador usar, 
no caso usamos o gcc mas pode ser usado outro caso necessário.
