#ifndef HEADER_H
#define HEADER_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

/* Estruturas */
typedef struct{
    char *palavra;
	int frequencia;
} DADOS_ARVORE;

typedef struct{
    struct ptNodo *palavra;
    int frequencia;
    double estatistica;
} DADOS_LISTA;

typedef struct ptArvore{
    DADOS_ARVORE dados;
    struct ptArvore *esq;
    struct ptArvore *dir;
} PT_ARVORE;

typedef struct ptLista{
    DADOS_LISTA dados;
    struct ptLista *prox;
} PT_LISTA;

typedef struct ArvorePalavra{
    // PENSAR MELHOR NOS CAMPOS
    struct ptArvore **qtdLetras;
    int maxLetras;
    struct ptArvore **acentoLetra;
    int maxLetraAcento;
} ARV_PALAVRAS;

/* Funcoes */

/* Funcao que iniciliza a arvore e retorna um ponteiro NULO*/
PT_ARVORE* inicializaArvore();

void le_linha(char *, int, FILE *);
PT_ARVORE* insereNodo(PT_ARVORE *r, const char *novo);
void imprimeOrd(PT_ARVORE *r);

#endif
