#include "busca.h"

#define PAL_POR_LINHA 10
int main(int argc, char *argv[]){
	FILE *input, *output;
	char separadores[] = " ,-.'!?)(:;0123456789/][<>{}|^~´`*&%$#@\n\t\r";
	char *tok;
	char buffer[256];
	int counter = 0;

	PT_ARVORE *abp = inicializaArvore();

	/* Abre o arquivo de entrada e le linha a linha
	em seguida separa as linhas por palavras e escreve
	na tela uma palavra de cada vez */
	if(argc != 3){
		printf("Parâmetros de entrada não estão como deveriam. [ARQ. ENTRADA] [ARQ. SAÍDA]\n");
		return 1;
	}
	if(!(input = fopen(argv[1], "r"))){
		printf("Erro ao abrir %s\n", argv[1]);
		return 2;
	}
	if(!(output = fopen(argv[2], "w"))){
		printf("Erro ao criar %s\n", argv[2]);
		return 3;
	}
	while(!feof(input)){
		if(fgets(buffer, sizeof(buffer), input)){
			tok = strtok(buffer, separadores);
			while(tok){
				//puts(tok);
				abp = insereNodo(abp, tok);
				fputs(tok, output);
				if(counter < PAL_POR_LINHA)
					fputs(" ", output);
				else{
					fputs("\n", output);
					counter = 0;
				}
				tok = strtok(NULL, separadores);
				counter ++;
			}
		}
	}
	imprimeOrd(abp);
	fclose(input);
	fclose(output);

	return 0;
}
